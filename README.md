Darts Simulator and Database
============================

A Python application for scraping, storing, simulating and modelling darts matches and results.


Installation
------------

After cloning the project, create a new [virtualenv][virtualenv] or [Conda env][conda] (using Python 3), activate it, then:

    pip install -r requirements-dev.txt -r requirements-cpython.txt

Next, copy the two files `.env.example` and `config.yml.example` to non-example locations:

    cp .env.example .env
    cp config.yml.example config.yml

Finally open the `config.yml` file and fill in the `DATABASE_URL`.


Usage
-----

### Querying

You can access the database using the `engine` and `Session` available in the `darts.db` module:

```
import pandas as pd

from darts.db import engine

df = pd.read_sql('SELECT * FROM match_results LIMIT 100', engine)
```

You can also make use of the database models available in the `darts.models` module:

```
from darts.db import Session
from darts.models import Player, Match, MatchResult

# Get all match results where the player's name is Phil Taylor
results = (
    Session.query(MatchResult)
    .join(Player)
    .join(Match)
    .filter(Player.name == "Phil Taylor")
    .all()
)
print(results[:5])
```

### Scraping new results

The scrapers run as Luigi tasks. You can schedule them both to run regularly using `python darts/etl/main.py`, or you can run them with custom parameters using:

    luigi --local-scheduler --module darts.etl.tasks.scrapers PlayerScraper
    luigi --local-scheduler --module darts.etl.tasks.scrapers EventScraper --year 2018  # or 2017, 2016, etc.


Examples
--------

There are some example queries in the `queries` directory - in particular the `matches_with_stats` one is useful.

There is an example of a modelling script in `explorations/first.py` which uses the above query, Pandas, and Scikit-Learn to predict the winner of a match using the following steps:

- get the data as `all_matches`
- create various categorical and numeric features to be used in modelling
- create a random train/test split from the data
- send the categorical and numeric features, and the outcome variable, through a Scikit-Learn pipeline which does feature preparation such as selecting columns, scaling, and one-hot encoding, then fits a Gradient Boosting classifier to the predictors
- runs a randomised search cross-validation to test a variety of hyperparameters, using distributions on the various parameters to describe how they should be sampled on each iteration
- puts the model scores into a DataFrame


Using odds
----------

So far the database doesn't contain any odds data. The `betfair.py` file (not yet integrated into the package) contains a class `BetfairMatchMarket` which takes a filename pointing to a Betfair historical market file and turns it into something useful. It can also match up to the players, matches and results in the database, using the `get_db_data` method. See the example at the bottom of the module.

Eventually it would be nice to have a separate set of tables for markets and odds of various occurrences. For example, we may have a many-to-many relationship between a `match_result` and a market - each market (e.g. 180s) applies to many `match_results`, and each `match_result` has many markets available. However, this would mean understanding and parsing a lot more of the Betfair files!


[virtualenv]: https://virtualenv.pypa.io/en/stable/userguide/#usage
[conda]: https://conda.io/docs/user-guide/tasks/manage-environments.html
