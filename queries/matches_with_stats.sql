WITH match_results AS (
    SELECT mr.match_id,
        mr.player_id,
        mr.score,
        AVG(average) OVER (PARTITION BY mr.player_id ORDER BY m.date ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) AS average_ever,
        AVG(average) OVER (PARTITION BY mr.player_id ORDER BY m.date ROWS BETWEEN 10 PRECEDING AND 1 PRECEDING) AS average_recent,
        AVG(oneeighties) OVER (PARTITION BY mr.player_id ORDER BY m.date ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) AS average_oneeighties,
        AVG(high_checkout) OVER (PARTITION BY mr.player_id ORDER BY m.date ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) AS average_high_checkout,
        AVG(checkout_percent) OVER (PARTITION BY mr.player_id ORDER BY m.date ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) AS average_checkout_percent,
        AVG(checkout_chances) OVER (PARTITION BY mr.player_id ORDER BY m.date ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) AS average_checkout_chances,
        ROW_NUMBER() OVER (PARTITION BY mr.match_id ORDER BY mr.player_id) AS player_num
    FROM match_results AS mr
    JOIN matches m ON mr.match_id = m.id
)
SELECT *,
    AVG(player_1_victor) OVER (PARTITION BY player_1_id ORDER BY match_date ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) AS player_1_prop_won,
    AVG(player_1_victor) OVER (PARTITION BY player_1_id ORDER BY match_date ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING) AS player_1_prop_won_last_5,
    AVG(player_1_victor) OVER (PARTITION BY player_1_id ORDER BY match_date ROWS BETWEEN 20 PRECEDING AND 1 PRECEDING) AS player_1_prop_won_last_20,
    AVG(1 - player_1_victor) OVER (PARTITION BY player_2_id ORDER BY match_date ROWS BETWEEN UNBOUNDED PRECEDING AND 1 PRECEDING) AS player_2_prop_won,
    AVG(1 - player_1_victor) OVER (PARTITION BY player_2_id ORDER BY match_date ROWS BETWEEN 5 PRECEDING AND 1 PRECEDING) AS player_2_prop_won_last_5,
    AVG(1 - player_1_victor) OVER (PARTITION BY player_2_id ORDER BY match_date ROWS BETWEEN 20 PRECEDING AND 1 PRECEDING) AS player_2_prop_won_last_20,
    LAG(sub.player_1_victor) OVER (PARTITION BY player_1_id ORDER BY match_date, match_id) AS player_1_won_prev_match,
    LAG(1 - sub.player_1_victor) OVER (PARTITION BY player_2_id ORDER BY match_date, match_id) AS player_2_won_prev_match
FROM (
    SELECT mr1.match_id,
        m.date AS match_date,
        e.name AS event_name,
        e.prize_fund AS prize_fund,
        mr1.player_id AS player_1_id,
        p1.name AS player_1_name,
        mr1.score AS player_1_score,
        mr1.average_ever AS player_1_average_ever,
        mr1.average_recent AS player_1_average_recent,
        mr1.average_oneeighties AS player_1_oneeighties,
        mr1.average_high_checkout AS player_1_high_checkout,
        mr1.average_checkout_percent AS player_1_checkout_percent,
        mr1.average_checkout_chances AS player_1_checkout_chances,
        mr2.player_id AS player_2_id,
        p2.name AS player_2_name,
        mr2.score AS player_2_score,
        mr2.average_ever AS player_2_average_ever,
        mr2.average_recent AS player_2_average_recent,
        mr2.average_oneeighties AS player_2_oneeighties,
        mr2.average_high_checkout AS player_2_high_checkout,
        mr2.average_checkout_percent AS player_2_checkout_percent,
        mr2.average_checkout_chances AS player_2_checkout_chances,
        CASE WHEN mr1.score > mr2.score THEN 1 ELSE 0 END AS player_1_victor
    FROM match_results AS mr1
    JOIN match_results AS mr2 ON mr1.match_id = mr2.match_id
    JOIN matches m ON mr1.match_id = m.id
    JOIN players p1 ON mr1.player_id = p1.id
    JOIN players p2 ON mr2.player_id = p2.id
    JOIN events e ON m.event_id = e.id
    WHERE mr1.player_id != mr2.player_id
        AND mr1.player_num = 1
) AS sub
ORDER BY match_date, player_1_name, player_2_name;
