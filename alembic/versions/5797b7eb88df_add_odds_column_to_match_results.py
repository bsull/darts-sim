"""Add odds column to match_results

Revision ID: 5797b7eb88df
Revises: 97964be29f94
Create Date: 2018-05-27 10:18:29.261979

"""

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '5797b7eb88df'
down_revision = '97964be29f94'
branch_labels = None
depends_on = None


def upgrade():
    op.add_column(
        'match_results',
        sa.Column('odds', sa.Float(), nullable=True),
    )


def downgrade():
    op.drop_column('match_results', 'odds')
