import datetime
import os

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from scipy.stats import beta, randint, uniform
import seaborn as sns
from sklearn import ensemble, model_selection, pipeline, preprocessing


os.environ['APP_SETTINGS_YAML'] = '../config.yml'


from darts.db import engine  # noqa
from utils import DataFrameSelector, PipelineFriendlyLabelBinarizer  # noqa


def df_from_query(query_name, conn=None):
    filename = '{}.sql'.format(query_name)
    path = os.path.join(os.pardir, 'queries', filename)
    with open(path) as infile:
        query = infile.read()
    return pd.read_sql(sql=query, con=engine)


all_matches = df_from_query('matches_with_stats')
all_matches.describe()
all_matches.tail()

matches = all_matches[all_matches.match_date >= datetime.date(2013, 1, 1)].dropna()

cat_features = [
    'player_1_name',
    'player_2_name',
]

num_features = [
    'player_1_average_ever',
    'player_1_average_recent',
    'player_2_average_ever',
    'player_2_average_recent',
    'player_1_oneeighties',
    'player_2_oneeighties',
    'player_1_high_checkout',
    'player_2_high_checkout',
    'player_1_checkout_percent',
    'player_2_checkout_percent',
    'player_1_checkout_chances',
    'player_2_checkout_chances',
    'player_1_prop_won',
    'player_1_prop_won_last_5',
    'player_1_prop_won_last_20',
    'player_2_prop_won',
    'player_2_prop_won_last_5',
    'player_2_prop_won_last_20',
]
outcome = 'player_1_victor'

matches[[outcome] + cat_features + num_features].corr()
matches_train, matches_test = model_selection.train_test_split(matches, test_size=0.2, random_state=1000)

num_pipeline = pipeline.make_pipeline(
    DataFrameSelector(num_features),
    preprocessing.StandardScaler(),
)


def make_cat_pipeline(feature):
    return pipeline.make_pipeline(
        DataFrameSelector(feature),
        PipelineFriendlyLabelBinarizer(),
    )


predictor_preparation_pipeline = pipeline.make_union(
    num_pipeline,
    *(make_cat_pipeline(f) for f in cat_features)
)
outcome_preparation_pipeline = pipeline.make_pipeline(
    DataFrameSelector([outcome]),
    PipelineFriendlyLabelBinarizer(),
)

full_pipeline = pipeline.make_pipeline(
    predictor_preparation_pipeline,
    ensemble.GradientBoostingClassifier(random_state=1000),
)

y_train = outcome_preparation_pipeline.fit_transform(matches_train).ravel()
y_test = outcome_preparation_pipeline.transform(matches_test).ravel()

param_distributions = {
    'gradientboostingclassifier__learning_rate': beta(a=2, b=10),
    'gradientboostingclassifier__n_estimators': randint(40, 200),
    'gradientboostingclassifier__max_depth': uniform(loc=1.5, scale=5),
    'featureunion__pipeline-1__dataframeselector__calculate_differences': [True, False],
}

clf = model_selection.RandomizedSearchCV(
    full_pipeline,
    param_distributions=param_distributions,
    scoring=['f1', 'neg_log_loss', 'accuracy'],
    cv=5, n_jobs=-1, verbose=2, refit='neg_log_loss', n_iter=50,
)
clf.fit(matches_train, y_train)

pd.DataFrame(clf.cv_results_).sort_values('mean_test_accuracy', ascending=False)
