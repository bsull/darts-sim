import sklearn


class PipelineFriendlyLabelBinarizer(sklearn.preprocessing.LabelBinarizer):
    def fit_transform(self, X, y=None):
        return super(PipelineFriendlyLabelBinarizer, self).fit_transform(X)


# Create a class to select numerical or categorical columns
# since Scikit-Learn doesn't handle DataFrames yet
class DataFrameSelector(
        sklearn.base.BaseEstimator,
        sklearn.base.TransformerMixin,
        ):
    def __init__(self, attribute_names, calculate_differences=False):
        self.attribute_names = attribute_names
        self.calculate_differences = calculate_differences

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        if not self.calculate_differences:
            return X[self.attribute_names].values
        seen = set()
        new_cols = []
        for original_col in self.attribute_names:
            col = original_col.replace('player_1_', '').replace('player_2_', '')
            if col not in seen:
                seen.add(col)
                new_cols.append(col)
        for col in new_cols:
            # New column will be positive if player 1 is better than player 2
            X[col] = X['player_1_{}'.format(col)] - X['player_2_{}'.format(col)]
        return X[new_cols].values
