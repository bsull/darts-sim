import json
from typing import Dict

from datetime import datetime
from dateutil.parser import parse
import pandas as pd
from sqlalchemy import func
from sqlalchemy.orm import aliased

from darts import db
from darts.models import Match, MatchResult, Player


def process_match_spec(row: dict):
    match_definition = row['mc'][0]['marketDefinition']
    runners = {
        runner['id']: runner['name']
        for runner in match_definition['runners']
    }
    return {
        'runners': runners
    }


def is_odds_revision(row: dict) -> bool:
    return 'rc' in row['mc'][0]


def process_odds_revisions(row: dict, match_definition: dict) -> dict:
    timestamp = datetime.fromtimestamp(row['pt']/1000)
    revisions = row['mc'][0]['rc']

    revisions_dict = {
        revision['id']: revision['ltp']
        for revision in revisions
    }
    revisions_dict['timestamp'] = timestamp

    return revisions_dict


class Runner:
    """
    A runner is something we could bet on, with some odds associated with
    it. It is generally identified using its id.
    """

    def __init__(self, id, name):
        self.id = id
        self.name = name
        self.is_tie = self.name.lower() == 'tie'

    def __str__(self):
        return f'{self.name}'

    def __repr__(self):
        return str(self)


class BetfairMatchMarket:
    """
    A representation of a Betfair market for a match.
    """
    def __init__(self, filename):
        self.filename = filename
        with open(filename) as f:
            self.all_rows = [json.loads(line) for line in f]

        self.all_mcs = [row['mc'][0] for row in self.all_rows]
        self.market_definition = self.all_mcs[0]['marketDefinition']
        self.market_type = self.market_definition['marketType']
        self.event_name = self.market_definition['eventName']
        self.match_time = parse(self.market_definition['marketTime'])

        # runners is a dictionary mapping runner ID to a runner
        self.runners = {
            r['id']: Runner(r['id'], r['name'])
            for r in self.market_definition['runners']
        }

        # odds_df is a dataframe containing the timestamps, runner ID and
        # latest odds for each odds update.
        match_def = process_match_spec(self.all_rows[0])
        self.odds_df = pd.DataFrame.from_records(
            process_odds_revisions(row, match_def)
            for row in self.all_rows
            if is_odds_revision(row)
        )

        # initial_odds is a mapping from runner ID to odds containing the
        # earliest 'last trade price' for each runner
        self.initial_odds: Dict[int, float] = (
            self.odds_df
            .fillna(method='bfill')
            .iloc[0]
            .drop('timestamp')
            .to_dict()
        )

        # These are unpopulated until `self.get_db_data` is called
        self.match = None
        self.match_results = None

    def __str__(self):
        return '{name} - {date} - {market}'.format(
            name=self.event_name,
            date=self.match_time.date(),
            market=self.market_type,
        )

    def __repr__(self):
        return '<BetfairMatchMarket({})>'.format(self)

    def get_db_data(self, session: db.Session) -> dict:
        """
        Query the database for the scraped match and match_results
        corresponding to this market. This will update:

            - the `match` attribute to contain the `Match`
            - the `match_results` attribute to contain a dictionary mapping
              runner ID to a `MatchResult` corresponding to the runner's
              results this match.
        """
        mr1 = aliased(MatchResult)
        p1 = aliased(Player)
        mr2 = aliased(MatchResult)
        p2 = aliased(Player)

        players = [
            runner
            for runner in self.runners.values()
            if not runner.is_tie
        ]

        match, match_result_1, match_result_2 = (
            session.query(Match, mr1, mr2)
            .join(mr1)
            .join(mr2, mr1.match_id == mr2.match_id)
            .join(p1, mr1.player_id == p1.id)
            .join(p2, mr2.player_id == p2.id)
            .filter(
                Match.date == self.match_time.date(),
                mr1.player_id != mr2.player_id,
            )
            .order_by(
                func.similarity(p1.name, players[0].name).desc(),
                func.similarity(p2.name, players[1].name).desc(),
            )
            .first()
        )

        self.match = match
        self.match_results = {
            players[0].id: match_result_1,
            players[1].id: match_result_2,
        }


# Test using something like this
s = db.Session()
bf_match = BetfairMatchMarket('data/betfair/MOST_180S/1.135924700')
bf_match.get_db_data(s)
bf_match.match
bf_match.match_results
bf_match.initial_odds
